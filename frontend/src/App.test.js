import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/learn react/i);
  const helloElement = screen.getByText('hello world', {exact: false})
  const TaskElement = screen.getByText(/Tasks/);
  const AddElement = screen.getByLabelText('Add');

  expect(linkElement).toBeInTheDocument();
  expect(helloElement).toBeInTheDocument();
  expect(TaskElement).toBeInTheDocument();
  expect(AddElement).toBeInTheDocument();

});


